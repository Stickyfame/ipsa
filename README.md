# PSA-project

## Dépendances

- Librairie armadillo (C++)
- numpy (Python3)
- matplotlib (Python3)
- setuptools (Python3)
- pyevtk (Python3)
- swig
- boost
- scipy (Python3)
- mongoDB
- pymongo (Python3)
- Doxygen

## Paramètres utilisateurs

Les entrées utilisateurs sont à fournir dans le fichier `src/inputs.json`. Liste des paramètres et leur signification :

- `xMax` et `yMax` (*unused*): valeur maximale de l'espace dans lequel on souhaite travailler. L'espace va alors de -xMax à xMax sur X et de -yMax à yMax sur Y.
- `nbValuesX` : nombre de valeurs sur l'axe X
- `nbValuesY` : nombre de valeurs sur l'axe Y (*unused*)
- `initialWave` : définit avec quelle fonction d'onde de départ on effectue les calculs, pour symboliser des phénomènes physiques différents. Les possibilités sont :
  - `"2D_HO"` pour les solutions de l'oscillateur harmonique à deux dimensions
  - `"YOUNG"` pour représenter une gaussienne avec vitesse initiale traversant des fentes d'Young
  - `"TUNNEL"` pour représenter une gaussienne avec vitesse initiale traversant un mur de potentiel
  - `"GAUSSIAN"` pour une gaussienne sans potentiel

- `method` : définit le schéma de différence finie utilisé pour le calcul des solutions. Les possibilités sont :
  - `FTCS` pour la méthode explicite Forward Time Center Space
  - `BTCS` pour la méthode implicite Backward Time Center Space
  - `CTCS` pour la méthode de Crank-Nicolson Center Time Center Space

- `gaussian` : un ensemble de paramètres propres pour créer une gaussienne comme état initial
  - `x0` et `y0` : le point de départ de la gaussienne
  - `kx` et `ky` : le vecteur vitesse de la gaussienne
  - `w` : la largeur de la gaussienne (*unused*)
  - `specificPotential` : pour appliquer un potentiel initial spécifique sous forme d'image (*unused*)

- `2D-HO` : regroupe des paramètres pour créer un état initial de solution 2D-HO
  - `rank` : le rang de la solutino harmonique

## Utilisation

Pour créer les composants requis, il suffit de lancer un `make` depuis le dossier `src`. Pour recompiler précisément les bindings ou la doc, utiliser `make bindings` ou `make doc`.

Pour ensuite utiliser le solveur, il faut lancer le fichier via `python3 src/solveur.py`. Le différents fichiers `.vtr` pour la visualisation se trouveront dans `src/res`.