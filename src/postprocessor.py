"""@package postprocessor
Module pour la conversion des matrices calculées au format .vtr pour la visualisation avec Paraview

Les différentes matrices à convertir en fichier .vtr se trouvent dans la base de données MongoDB.
"""
import numpy as np
from pyevtk.hl import gridToVTK

#TODO: se connecter à la DB et créer tous les .vtr correspondant ?
# comment choisir si on créé que le .vtr de la matricie initiale ou aussi des itérations ?

def toVtr(mat, n, range, filename):
    """Fonction de conversion d'une matrice en un fichier .vtr
    """
    Z = np.linspace(0.0, 1.0, 1, dtype=np.float64)
    N = np.array(mat, dtype = complex, order = 'C')
    PSI = np.absolute(N).reshape((n, n, 1))
    X = np.linspace(-range,range,n,endpoint=False)

    gridToVTK(filename, X, X, Z, pointData = {'N': np.array(PSI, order = 'C')})
