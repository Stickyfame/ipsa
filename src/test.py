#!/usr/bin/env python3
#FIXME: ATTENTION LE import cppbind IMPORTE LE FICHIER .py AU LIEU DU MODULE

import cppbind
import numpy as np

numpyArray = np.array([[0.1+0.j, 0.2+0.j], [0.3+0.j, 0.4+0.j]], dtype = complex, order = 'F')
numpyArrayR = np.array([[0.1, 0.2], [0.3, 0.4]], dtype = np.float64, order = 'F')

print("Initial object:")
print(numpyArray)

mc = cppbind.Cppbind(1, 1, numpyArray, numpyArrayR)
print("Psi in the C++ class (constructor with argument):")
print(mc.myMat)

print("Potentiel in the C++ class (constructor with argument):")
print(mc.myPot)

print("Call to FTCS function (should return 3x3 matrix filled with ones):")
print(mc.ftcs(numpyArray))

print("Call to BTCS function (should return 3x3 matrix filled with ones):")
print(mc.btcs(numpyArray))

print("Call to CTCS function (should return 3x3 matrix filled with ones):")
print(mc.ctcs(numpyArray))

print("Call to norme function :")
norme = mc.norme(numpyArray, numpyArray)
if norme == 0.0:
    print("norme OK")
else :
    print("norme KO: {} vs {}".format(norme, 0.0))
