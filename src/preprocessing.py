"""@package preprocessing
Module python du Field Generator

Contient les différentes fonctions pour générer les fonctions d'ondes et potentiels initiaux.
Les différentes générations possibles sont : les solutions 2D-HO, les fentes d'Young et l'effet tunnel.
"""
import numpy as np
import math
import cmath
from scipy import constants
import json
from pyevtk.hl import gridToVTK
#import pour Mongo
from pymongo import MongoClient
import pymongo.errors
import bson
import pickle
import hashlib
from datetime import datetime

#Variable globale
file = "./inputs.json" #emplacement du fichier inputs.json

##__DEBUT FONCTIONS MONGO

#fonction pour obtenit le hash du fichier JSON
def get_hash():
    with open(file) as f:
        file_data = json.load(f)
    f.close()
    file_data = json.dumps(file_data, sort_keys=True)
    file_hash = hashlib.md5(file_data.encode("utf-8")).hexdigest() #HASH du fichier json
    return(file_hash)

# première fonction pour initialiser le run dans la table MongoDB
def run_isnew():
    file_hash = get_hash()
    username, password, host, dbname = 'user', 'password', '127.0.0.1', 'ipsa_project'
    client = MongoClient('mongodb://%s:%s@%s/%s' % (username, password, host, dbname))
    return client['ipsa_project']['restart'].find_one({'_id':file_hash})
        
def init_mongo():
    if run_isnew()==None:  #il n'y a pas de document existant avec ce hash
        #Connexion à la base de données
        username, password, host, dbname = 'user', 'password', '127.0.0.1', 'ipsa_project'
        client = MongoClient('mongodb://%s:%s@%s/%s' % (username, password, host, dbname))
        file_hash= get_hash()
        #assignation des variables à stocker pour faciliter la lecture
        with open(file) as f:
            file_data = json.load(f)
        f.close()
        mat = np.eye(3) #initialisation -> à remplacer par la matrice initiale
        potentiel = np.eye(3) #initialisation -> à remplacer par le potentiel
        
        #gestion des erreurs avec try/except
        try:
            bindat = bson.binary.Binary(pickle.dumps(mat, protocol = 2)) #encodage pour pouvoir stocker la matrice dans la bdd
            poten = bson.binary.Binary(pickle.dumps(potentiel, protocol = 2))
            data_id = client['ipsa_project']['restart'].insert_one({'_id':file_hash,'json':file_data,'mat': bindat, 'potentiel' : poten,'time':datetime.utcnow()}).inserted_id
        except pymongo.errors.OperationFailure as e:
            print("ERROR: %s" % (e))
    else : #il y a une trace existante pour les inputs renseignées
        #Connexion à la base de données
        file_hash = get_hash()
        username, password, host, dbname = 'user', 'password', '127.0.0.1', 'ipsa_project'
        client = MongoClient('mongodb://%s:%s@%s/%s' % (username, password, host, dbname))       
        #Recuperation des elements du dernier run avec les donnée du json
        
        try:
            for run in client['ipsa_project']['restart'].find({"_id":file_hash}):
                data_id = run["_id"]
                json = run["json"]
                bindat = run["mat"]
                poten = run["potentiel"]
                mat = pickle.loads(bindat)
                potentiel = pickle.loads(potentiel)
                time = run["time"]
                return(data_id,json,mat,potentiel,time)
        except pymongo.errors.OperationFailure as e:
            print("ERROR: %s" % (e))
        
#Fonction pour enregistrer la matrice et le potentiel donnés en paramètres dans la BDD MongoDB
def push_mongo(mat,potentiel):
    #connexion  au serveur mongo
    username, password, host, dbname = 'user', 'password', '127.0.0.1', 'ipsa_project'
    client = MongoClient('mongodb://%s:%s@%s/%s' % (username, password, host, dbname))
    #récupération du hash de inputs.json
    file_hash = get_hash()
    #mise à jour des éléments dans la bdd mongo
    try:
        new_bindat = bson.binary.Binary(pickle.dumps(mat, protocol = 2)) #encodage pour pouvoir stocker la matrice dans la bdd
        new_poten = bson.binary.Binary(pickle.dumps(potentiel, protocol = 2))
        client['ipsa_project']['restart'].update_one({'_id':file_hash},{'$set': {'mat':new_bindat, 'potentiel':new_poten, 'time':datetime.utcnow()}})
    except pymongo.errors.OperationFailure as e:
        print("ERROR: %s" % (e))

#Fonction pour récupérer les inputs, la matrice et le potentiel pour un hash donné 
def pull_mongo():
    #connexion  au serveur mongo
    username, password, host, dbname = 'user', 'password', '127.0.0.1', 'ipsa_project'
    client = MongoClient('mongodb://%s:%s@%s/%s' % (username, password, host, dbname))
    #récupération du hash de inputs.json
    file_hash = get_hash()
    try:
        for run in client['ipsa_project']['restart'].find({"_id":file_hash}):
            json = run["json"]
            bindat = run["mat"]
            poten = run["potentiel"]
            mat = pickle.loads(bindat)
            potentiel = pickle.loads(poten)
            return(json,mat,potentiel)
    except pymongo.errors.OperationFailure as e:
        print("ERROR: %s" % (e))
    
##__FIN FONCTIONS MONGO

#FIXME: fonction qui devrait être en postprocessing
def toVtr(mat, n, range, filename):
    Z = np.linspace(0.0, 1.0, 1, dtype=np.float64)
    N = np.array(mat, dtype = complex, order = 'C')
    PSI = np.absolute(N).reshape((n, n, 1))
    X = np.linspace(-range,range,n,endpoint=False)

    gridToVTK(filename, X, X, Z, pointData = {'N': np.array(PSI, order = 'C')})


def dho() :
    """Génération des matrices initiales pour les solutions 2D-HO

    Génère à partir des entrées de inputs.json la fonction d'onde initiale d'une solution 2D-HO ainsi que le potentiel initial.
    Retourne le couple de matrices (fonction d'onde, potentiel).
    """

    with open('inputs.json') as json_file :
        inputs = json.load(json_file)
        nbValues = inputs['nbValuesX'] # le nombre de valeurs en lesquelles on évalue la solution
        nMax = inputs['2D-HO']['rank'] # le rang de la solution
        iMax = inputs['xMax'] # jusqu'où on évalue sur l'axe X les solutions

    m = 1.675*10**-27
    omega = 10**-5

    coefs = []
    psi = []
    res = []
    potentiel = []
    # on initialise le vecteurs des valeurs sur x
    X = np.linspace(-1,iMax,nbValues,endpoint=False)

    print("Génération de la fonction d'onde et du potentiel initiaux 2D-HO...")

    for i in X :
        coefs.append( 1/math.sqrt(2**nMax * math.factorial(nMax)) * math.pow(m*omega/math.pi/constants.hbar,1/4) * math.exp(-m*omega*i**2/2/constants.hbar) )

    for i in range(nbValues) :
        psi.append(np.polynomial.hermite.Hermite(coef=[0] * (nMax-1) + [coefs[i]])(math.sqrt(m*omega/constants.hbar) * X[i]) )

    # psi contient maintent toutes les valeurs psi_nMax(x) pour x entre 0 et iMax
    # on construit la matrices en multipliant psi(x) et psi(y) comme dit dans les slides
    for i in range(nbValues) :
        res.append([])
        potentiel.append([])
        for j in range(nbValues) :
            res[i].append(complex(psi[i]*psi[j], 0))
            potentiel[i].append(1/2 * m*omega**2*(X[i]**2+X[j]**2))

    json_file.close()

    print("init OK")

    #TODO: appeler la fonction pour stocker inputs et res et potentiel
    push_mongo(res,potentiel)

    print("Génération du .vtr initial ...")
    filename = 'hdo_init_grid'
    Z = np.linspace(0.0, 1.0, 1, dtype=np.float64)
    N = np.array(res, dtype = complex, order = 'C')
    PSI = np.real(N).reshape((nbValues, nbValues, 1))

    gridToVTK(filename, X, X, Z, pointData = {'N': np.array(PSI, order = 'C')})
    print(".vtr OK")

    return(np.array(res, dtype = complex, order = 'F')/np.real(res).max(),np.array(potentiel, dtype = np.float64, order = 'F'))

def gaussian() :
    """Génération de la matrice initiale pour un paquet gaussien sans potentiel

    Génère à partir des entrées de inputs.json la fonction d'onde initiale d'un paquet gaussien sans potentiel.
    Retourne le couple de matrices (fonction d'onde, potentiel).
    """

    potentiel = []
    res = []

    with open('inputs.json') as json_file :
        inputs = json.load(json_file)
        nbValues = inputs['nbValuesX'] # le nombre de valeurs en lesquelles on évalue la solution
        iMax = inputs['xMax'] # jusqu'où on évalue sur l'axe X les solutions
        x0 = inputs['gaussian']['x0']
        y0 = inputs['gaussian']['y0']
        kx = inputs['gaussian']['kx']
        ky = inputs['gaussian']['ky']
        w = 1e-1 # meilleur paramètre jusqu'à maintenant pour que ce soit joli au rendu ...

    X = np.linspace(-iMax,iMax,nbValues,endpoint=False)

    print("Génération de la fonction d'onde et du potentiel initiaux pour un paquet gaussien...")

    for i in range(nbValues) :
        res.append([])
        potentiel.append([])
        for j in range(nbValues) :
            x = X[i]
            y = X[j]
            rho = -((x-x0)**2 + (y-y0)**2 ) / w**2
            theta = kx*x+ky*y
            res[i].append(
                cmath.exp(complex(rho, theta))
            )
            potentiel[i].append(0)

    print("init OK")

    #TODO: appeler la fonction pour stocker inputs et res et potentiel
    push_mongo(res,potentiel)

    return(np.array(res, dtype = complex, order = 'F'), np.array(potentiel, dtype = np.float64, order = 'F'))


def young() :
    """Génération des matrices initiales pour les fentes d'Young

    Génère à partir des entrées de inputs.json la fonction d'onde initiale pour l'expérience des fentes d'Young ainsi que le potentiel initial.
    Retourne le couple de matrices (fonction d'onde, potentiel).
    """
    #FIXME: le potentiel provoque une réaction étrange de la gaussienne en FTCS : téléportation sur le potentiel

    potentiel = []
    res = []

    with open('inputs.json') as json_file :
        inputs = json.load(json_file)
        nbValues = inputs['nbValuesX'] # le nombre de valeurs en lesquelles on évalue la solution
        iMax = inputs['xMax'] # jusqu'où on évalue sur l'axe X les solutions
        x0 = inputs['gaussian']['x0']
        y0 = inputs['gaussian']['y0']
        kx = inputs['gaussian']['kx']
        ky = inputs['gaussian']['ky']
        w = 1e-1 # meilleur paramètre jusqu'à maintenant pour que ce soit joli au rendu ...

    X = np.linspace(-iMax,iMax,nbValues,endpoint=False)

    print("Génération de la fonction d'onde et du potentiel initiaux pour les fentes d'Young...")

    for i in range(nbValues) :
        res.append([])
        potentiel.append([])
        for j in range(nbValues) :
            x = X[i]
            y = X[j]
            rho = -((x-x0)**2 + (y-y0)**2 ) / w**2
            theta = kx*x+ky*y
            res[i].append(
                cmath.exp(complex(rho, theta))
            )
            if (j > nbValues/2-2 and j < nbValues/2+2):
                if (i < nbValues/2-5 and i > nbValues/2-15):
                    potentiel[i].append(0)
                elif (i > nbValues/2+5 and i < nbValues/2+15):
                    potentiel[i].append(0)
                else:
                    potentiel[i].append(0.01)
            else:
                potentiel[i].append(0)

    return(np.array(res, dtype = complex, order = 'F'), np.array(potentiel, dtype = np.float64, order = 'F'))

def tunnel() :
    """Génération des matrices initiales pour l'effet tunnel

    Génère à partir des entrées de inputs.json la fonction d'onde initiale pour l'expérience de l'effet tunnel ainsi que le potentiel initial.
    Retourne le couple de matrices (fonction d'onde, potentiel).
    """
    #TODO: génération des matrices
    #TODO: entrées utilisateurs pour les paramètres de tunnel
    print("Matrices initiales pour l'effet tunnel à implémenter")
    potentiel = []
    res = []
    return(np.array(res, dtype = complex, order = 'F'), np.array(potentiel, dtype = np.float64, order = 'F'))
