"""@package solver
Module pour le calcul des solutions de l'équation de Schrödinger non-relativiste dépendante du temps, en deux dimensions

Le calcul de la propagation en fonction du temps peut s'effectuer via différents schéma de différences finies.
Les schémas disponibles sont : FTCS, BTCS et CTCS. Les calculs font appel à du code C++ et à la librairie armadillo.
Les résultats sont stockés dans une base de données MongoDB.
"""
import json
import preprocessing
import numpy as np
import matplotlib.pyplot as plt
import cppbind

psi, V = np.array([]), np.array([])

def ftcs(myClass, mat) :
    """Schéma FTCS : Forward Time, Center Space
    """
    return myClass.ftcs(mat)

def btcs(myClass, mat) :
    """Schéma BTCS : Backward Time, Center Space
    """
    return myClass.btcs(mat)

def ctcs(myClass, mat) :
    """Schéma CTCS : Center Time, Center Space
    """
    return myClass.ctcs(mat)

def iter(init_psi, init_V, f, dx, dy, dt=10, epsilon=0.000001):
    """Fonction principale pour lancer le calcul de chaque itération et stocker le résultat
    """
    print("Itérations de la propagation dynamique...")
    mass = 1.675*10**-27
    myClass = cppbind.Cppbind(dt, mass, dx, dy, init_psi, init_V)
    norme = np.inf
    old = init_psi
    num_iter = 0
#    plt.matshow(np.absolute(init_psi))
#    plt.matshow(init_V)
#    plt.colorbar()
#    plt.show()
    while norme > epsilon :
        res = f(myClass, old)
        norme = myClass.norme(res, old)
        old = np.copy(res)
        num_iter += 1

        #print(norme)
        
        if (num_iter % 1000 == 0) :
            print("itération {}, norme = {}".format(num_iter,norme))
        #    plt.matshow(np.real(res))
        #    plt.colorbar()
        #    plt.show()
            #TODO: stocker ici la matrice res dans la BDD

            preprocessing.toVtr(np.absolute(res), 100, 1, "res/iter_{}".format(num_iter))

#TODO: Pas besoin d'ouvrir les inputs pour avoir PSI et V : récupérer le hash via preprocessing
# et récupérer les matrices dans Mongo. Peut-être mettre le hash en global, ou en paramètres de la
# fonction iter
with open('inputs.json') as json_file:
    inputs = json.load(json_file)
    method = inputs['method']
    initial = inputs['initialWave']
    dx = inputs['xMax'] / inputs['nbValuesX'] * 2
    dy = dx

# Utilisation de Mongo
#json,psi,V = proprocessing.pull_mongo()

if initial == "2D-HO":
    psi, V = preprocessing.dho()
    dx = dx / 2
    dy = dy / 2
elif initial == "YOUNG":
    psi, V = preprocessing.young()
elif initial == "TUNNEL":
    psi, V = preprocessing.tunnel()
elif initial == "GAUSSIAN":
    psi, V = preprocessing.gaussian()
else :
    print("Erreur de fonction initale dans le fichier inputs.json .")
    print("Veuillez vérifier que l'initialWave soit 2D-HO, YOUNG ou TUNNEL.")
    exit(1)
    
# Cette partie reste pareil avec la modif MongoDB
if method == "FTCS":
    iter(psi, V, ftcs, dx, dy)
elif method == "BTCS":
    iter(psi, V, btcs, dx, dy)
elif method == "CTCS":
    iter(psi, V, ctcs, dx, dy)
else:
    print("Erreur de méthode dans le fichier inputs.json.")
    print("Veuillez vérifier que la méthode soit FTCS, BTCS ou CTCS.")
    exit(1)
