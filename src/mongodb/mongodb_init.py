#!/usr/bin/env python3

import pymongo

#on récupére le serveur local de mongodb 
client = pymongo.MongoClient("mongodb://127.0.0.1/")

#on regarde si la db existe et on la crée avec un user sinon
dblist = client.list_database_names()
if "ipsa_project" in dblist:
  print("The database already exists.")
  exit()
else :
    db = client["ipsa_project"]
    print("db ips_project created")
    db.command("createUser", "user", pwd="password", roles=["dbOwner"])
    print("user created")

#on regarde si il existe la collection et on la crée sinon
collist = db.list_collection_names()
if "restart" in collist:
  print("The collection 'restart' allready exists.")
  exit()
else :
    col = db["restart"]
    print("restart collection created")


