#!/usr/bin/env python3

from pymongo import MongoClient
import pymongo.errors
import numpy as np
import bson
import pickle
import hashlib
import json
from datetime import datetime

file = "../inputs.json"
BLOCK_SIZE = 65536 # taille du buffer de lecture pour ne pas essayer de charger tout le doc en mémoire , pas nécessaire ici mais pas plus chere

file_hash = hashlib.md5()
with open(file, 'rb') as f: # ouverture du fichiers
    fb = f.read(BLOCK_SIZE) # Lecture du fichier par block
    while len(fb) > 0: # Tant que le buffer est pas videe
        file_hash.update(fb) # mise à jour du hash
        fb = f.read(BLOCK_SIZE) # on passe au block suivant

#print (file_hash.hexdigest()) # le hash du fichier sous forme de string
#print(file_hash.digest()) #  le hash du fichier sous forme de bytes

#__STOCKAGE DANS MONGO

#on récupére le serveur local de mongodb
username, password, host, dbname = 'user', 'password', '127.0.0.1', 'ipsa_project'
client = MongoClient('mongodb://%s:%s@%s/%s' % (username, password, host, dbname))
#assignation des variables à stocker pour faciliter la lecture
with open(file) as f:
    file_data = json.load(f)
f.close()
cle = file_hash.hexdigest()
mat = np.eye(3) * 2
potentiel = np.eye(3)
#gestion des erreurs avec try/except
try:
    bindat = bson.binary.Binary(pickle.dumps(mat, protocol = 2)) #encodage pour pouvoir stocker la matrice dans la bdd
    poten = bson.binary.Binary(pickle.dumps(potentiel, protocol = 2))
    data_id = client['ipsa_project']['restart'].insert_one({'_id':cle,'json':file_data,'mat': bindat, 'potentiel' : poten,'time':datetime.utcnow()}).inserted_id
except pymongo.errors.OperationFailure as e:
    print("ERROR: %s" % (e))

#__RECUPERATION DONNEES

try:
    for data in client['ipsa_project']['restart'].find({"_id":cle}):
        bindat = data["mat"]
        data_id = data["_id"]
        mat = pickle.loads(bindat)
        time = data["time"]
        print("id: %s" % (str(data_id)))
        print(mat)
        print(time)
        print("EOF")
except pymongo.errors.OperationFailure as e:
  print("ERROR: %s" % (e))

print("Table avant modifs")
for d in client['ipsa_project']['restart'].find(): print(str(d))
##__FIND AND REPLACE
new_mat = np.eye(3) * 3
try:
    new_bindat =  bson.binary.Binary(pickle.dumps(new_mat, protocol = 2))
    client['ipsa_project']['restart'].update_one({'_id':cle},{'$set': {'mat':new_bindat, 'time':datetime.utcnow()}})
    print("table après modifs")
    for d in client['ipsa_project']['restart'].find(): print(str(d))
except pymongo.errors.OperationFailure as e:
  print("ERROR: %s" % (e))

client.close
