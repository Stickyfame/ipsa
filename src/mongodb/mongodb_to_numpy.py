#!/usr/bin/env python3

from pymongo import MongoClient
import pymongo.errors
import numpy as np
import bson
import pickle

username, password, host, dbname = 'user', 'password', '127.0.0.1', 'ipsa_project'
client = MongoClient('mongodb://%s:%s@%s/%s' % (username, password, host, dbname))
try:
  for i in range(3):
    mat = np.eye(3) * i
    bindat = bson.binary.Binary(pickle.dumps(mat, protocol = 2))
    data_id = client['ipsa_project']['db_array'].insert_one({'mat': bindat}).inserted_id
    print("id: %s" % (str(data_id))) #on peut enlever cette partie si on ne veut pas afficher les matrices)
    print(mat) #idem
except pymongo.errors.OperationFailure as e:
  print("ERROR: %s" % (e))
