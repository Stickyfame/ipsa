# MongoDb IPSA

Explication rapide de l'utilisation des fichiers `mongodb_init.py` et `mongodb_to_numpy.py`.
Prérequis:

* Python3
* MongoDb installé et démarré.
* package pymongo installé

Fichier de conf local mongodb utilisé sur mon ordi :

systemLog:
  destination: file
  path: /usr/local/var/log/mongodb/mongo.log
  logAppend: true
storage:
  dbPath: /usr/local/var/mongodb
  journal:
    enabled: false
net:
  bindIp: 127.0.0.1
  port: 27017
security:
  authorization: enabled  //ici faut voir si on disable ou enable

Le fichier `mongodb_init.py` va initialiser une BDD Mongo vide avec :

* db_name = `ipsa_project`
* user_id = `user`
* user_password = `password`
* roles=["dbOwner"]

Dans cette DB , on créé une collection `db_array`

Le fichier `mongodb_to_numpy.py` se connecte à cette db et ajoute les matrices numpy crées dans le fichier même et les affiche.

Pour exécuter les scripts :
```
python3 mongodb_init.py
python3 mongodb_to_numpy.py
```