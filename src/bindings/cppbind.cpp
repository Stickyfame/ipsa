#include "cppbind.h"

/**
 * \file cppbind.cpp
 * \author Anteunis Charles
 * \brief Code des différentes méthodes de différences finies pour le calcul des itérations. Les itérations sont calculées à l'aide de la fonction suivante, qu'on appelle g :
 */

/**
 * \fn Cppbind::ftcs
 * \brief Méthode de différence finie explicite FTCS
 * \param mat matrice de psi à l'itération n
 * \return la matrice de psi à l'itération n+1
 */
arma::cx_mat Cppbind::ftcs(arma::cx_mat mat) {
    arma::cx_mat res(arma::size(mat));
    int n = mat.n_rows;
    int nc = mat.n_cols;
    const double hbar = arma::datum::h_bar;
    arma::cx_double mat_ip1 = 0, mat_im1 = 0, mat_jp1 = 0, mat_jm1 = 0;

    for (int i = 0 ; i<n ; i++) {
        for (int j = 0; j<nc ; j++)
        {            
            // Les conditions aux limites
            if (i == 0) {
                mat_im1 = 0;
                mat_ip1 = mat(i+1,j);
            }
            else if (i == n-1) {
                mat_im1 = mat(i-1,j);
                mat_ip1 = 0;
            }
            else {
                mat_im1 = mat(i-1,j);
                mat_ip1 = mat(i+1,j);
            }
            if (j == 0) {
                mat_jm1 = 0;
                mat_jp1 = mat(i,j+1);
            }
            else if (j == nc-1) {
                mat_jm1 = mat(i,j-1);
                mat_jp1 = 0;
            }
            else {
                mat_jp1 = mat(i,j+1);
                mat_jm1 = mat(i,j-1);
            }
            
            res(i,j) = 
                arma::cx_double(0,-dt/hbar) * (
                    -hbar*hbar/(2*m*dx*dx) * (mat_ip1 + mat_im1)
                    -hbar*hbar/(2*m*dy*dy) * (mat_jp1 + mat_jm1)
                    +mat(i,j) * (hbar*hbar/(m*dx*dx) + hbar*hbar/(m*dy*dy) + myPot(i,j) + arma::cx_double(0,hbar/dt))
                );
        }
        
    }
    return res;
}

/**
 * \fn Cppbind::btcs
 * \brief Méthode de différence finie implicite BTCS
 * \param mat matrice de psi à l'itération n
 * \return la matrice de psi à l'itération n+1
 */
arma::cx_mat Cppbind::btcs(arma::cx_mat mat) {
    arma::cx_mat zero = arma::cx_mat(mat.n_rows + 2, mat.n_cols + 2, arma::fill::zeros);
    
    zero.submat(1, 1, mat.n_rows, mat.n_cols) = mat;
    arma::cx_mat tmp = arma::cx_mat(zero);

    arma::cx_mat pv, res = tmp;
    // preparation du decalage
    arma::cx_mat offset = arma::cx_mat(tmp.n_rows, tmp.n_cols, arma::fill::zeros);
    for (arma::uword i = 0; i < offset.n_cols-1; i++)
    {
        offset(i+1, i) = 1;
        offset(i, i+1) = 1;
    }

    // preparation de la matrice potentiel
    /* zero.submat(1, 1, mat.n_rows, mat.n_cols) = myPot;
    arma::cx_mat pot = arma::cx_mat(zero); FIXME - type error */
    myPot.insert_rows(0, 1);
    myPot.insert_cols(0, 1);
    myPot.insert_rows(myPot.n_rows, 1);
    myPot.insert_cols(myPot.n_cols, 1);
    
    double norm = 1;
    const double hbar = arma::datum::h_bar;
    arma::cx_double i_cx = arma::cx_double(0., 1.);

    int tmp_n = tmp.n_cols - 2;
    while (norm > 10e-15)
    {
        pv = res;

        arma::cx_mat mat1 = -1.0*hbar*hbar*(offset*pv)/(2*m*dx*dx);
        arma::cx_mat mat2 = -1.0*hbar*hbar*(pv*offset)/(2*m*dy*dy);
        arma::cx_mat mat3 = i_cx*hbar*res/dt;

        arma::cx_mat factor = (i_cx*hbar/dt - myPot - hbar*hbar/(m*dx*dx) - hbar*hbar/(m*dy*dy));

        res.submat(1,1,tmp_n,tmp_n) = (mat1.submat(1,1,tmp_n,tmp_n) + mat2.submat(1,1,tmp_n,tmp_n) + mat3.submat(1,1,tmp_n,tmp_n))/factor;

        norm = Cppbind::norme(pv.submat(1,1,tmp_n,tmp_n), res.submat(1,1,tmp_n,tmp_n));
    }

    return res.submat(1,1,tmp_n,tmp_n);
}

/**
 * \fn Cppbind::ctcs
 * \brief Méthode de différence finie explicite CTCS
 * \param mat matrice de psi à l'itération n
 * \return la matrice de psi à l'itération n+1
 */
arma::cx_mat Cppbind::ctcs(arma::cx_mat mat) {
    arma::cx_mat zero = arma::cx_mat(mat.n_rows + 2, mat.n_cols + 2, arma::fill::zeros);
    
    zero.submat(1, 1, mat.n_rows, mat.n_cols) = mat;
    arma::cx_mat tmp = arma::cx_mat(zero);
    
    // preparation du decalage
    arma::cx_mat offset = arma::cx_mat(tmp.n_rows, tmp.n_cols, arma::fill::zeros);
    for (arma::uword i = 0; i < offset.n_cols-1; i++)
    {
        offset(i+1, i) = 1;
        offset(i, i+1) = 1;
    }

    arma::cx_mat pv, res = tmp;
    double norm = 1;
    const double hbar = arma::datum::h_bar;
    arma::cx_double i_cx = arma::cx_double(0., 1.);

    // preparation des potentiels
    myPot.insert_rows(0, 0);
    myPot.insert_cols(0, 0);
    myPot.insert_rows(myPot.n_rows+1, 0);
    myPot.insert_cols(myPot.n_cols+1, 0);

    while (norm > 10e-15)
    {
        pv = res;

        arma::cx_mat mat1 = -1.0*hbar*hbar*(offset*pv)/(2*m*dx*dx);
        arma::cx_mat mat2 = -1.0*hbar*hbar*(pv*offset)/(2*m*dy*dy);
        arma::cx_mat mat3 = -1.0*hbar*hbar*(offset*tmp)/(2*m*dx*dx);
        arma::cx_mat mat4 = -1.0*hbar*hbar*(tmp*offset)/(2*m*dy*dy);
        arma::cx_mat mat5 = (myPot + 2.0*i_cx*hbar/dt + hbar*hbar/(m*dx*dx) + hbar*hbar/(m*dy*dy))%tmp;

        arma::cx_mat factor = 2.0*i_cx*hbar/dt - myPot - hbar*hbar/(m*dx*dx) - hbar*hbar/(m*dy*dy);

        res = (mat1 + mat2 + mat3 + mat4 + mat5)/factor;

        norm = Cppbind::norme(pv, res);
    }

    return res;
}

/**
 * \brief Calcul de la norme de la différence entre deux itérations de g pour stopper l'itération selon un epsilon particulier
 * \param mat_before matrice de g à l'itération n
 * \param mat_after  matrice de g à l'itération n+1
 * \return la norme de a - b
 */
double Cppbind::norme(arma::cx_mat a, arma::cx_mat b) {
    return arma::norm(a - b);
}

/**
 * \fn Cppbind - constructeur
 * \brief Constructeur de la classe Cppbind, assigne les valeurs initiales dx, dy, dt, m et la matrice de potentiel
 */
Cppbind::Cppbind(double _dt, double _m, double _dx, double _dy, arma::cx_mat _mat, arma::mat _pot) : dt(_dt), m(_m), dx(_dx), dy(_dy), myMat(_mat), myPot(_pot)
{
}
