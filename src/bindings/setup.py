from setuptools import setup, Extension
import numpy

#FIXME: LE MODULE PYTHON cppbind A LE MEME NOM QUE cppbind.py, ATTENTION NE PAS FAIRE UN import cppbind DANS LE DOSSIER bindings/

module1 = Extension('_cppbind',
                    include_dirs = ['./include/armanpy/',numpy.get_include(),'/usr/local/include'],
                    libraries = ['m', 'z', 'armadillo'],
                    sources = ['cppbind.i', 'cppbind.cpp'],
                    library_dirs = ['/usr/local/lib'],
                    swig_opts = ["-c++", "-Wall", "-I.", "-I./include/armanpy/"])

setup (name = 'package_test',
       py_modules = ['cppbind'],
       version = '1.0',
       description = 'This is a test package',
       ext_modules = [module1],
       include_dirs=[numpy.get_include()])

